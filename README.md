# chaturresume

## This is simple dummy Job Portfolio made using vue.js framework. <br> Clone it, make it according to your requirements, hope you gonna like it :)

## Use this command to 
**Project setup** <br>
**Compiles and hot-reloads for development** <br> 
**Compiles and minifies for production** <br>
**Lints and fixes files**

```
npm install
npm run serve
npm run build
npm run lint
```
